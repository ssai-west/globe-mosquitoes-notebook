# import pyautogui, PIL, and sleep for taking screenshots
#import pyautogui
import pyscreenshot as ImageGrab
from PIL import Image as Picture
from time import sleep

# PyAutoGUI==0.9.48
# ScreenCapture==0.1.0
# PyGTK==2.24.0
# bokeh==1.4.0


# pdf maker code, not implemented due to driver issues
# might work in hub system
designate('scanning notebook subroutine')

# scan notebook for cell information
def scan():
    """Scan the notebook and collect cell information.

    Arguments:
        None

    Returns:
        list of dicts
    """

    # open the notebook file and read its contents
    with open('mosquitoes.ipynb', 'r') as pointer:
        contents = json.loads(pointer.read())

    # get all cells
    cells = contents['cells']

    return cells


designate('revealing open cells subroutine')

# discover which cells are hidden and which are not
def reveal(cells):
    """Reveal which cells are in a hidden state and which are not.

    Arguments:
        cells: list of dicts

    Returns:
        list of ints
    """

    # keep only non-hidden cells
    visibles = []
    for index, cell in enumerate(cells):

        # get hidden, hide_input, and output states
        hidden = cell['metadata'].setdefault('hidden', False)
        collapsed = cell['metadata'].setdefault('hide_input', False)
        outputs = 'outputs' in cell.keys()

        # add to opens if both are false
        if hidden is False and collapsed is False or outputs:
            # add to opens
            visibles.append(index)

    return visibles


# bookmark which cells to scroll to
def bookmark(cells, visibles):
    """Bookmark which cells to scroll to.

    Arguments:
        cells: list of dicts
        visibles: list of ints

    Returns:
        list of ints
    """

    # set total line length for new page
    criterion = 20

    # set dummy accumulation for first cell
    lines = 0
    accumulation = criterion + 1

    # determine scroll indices
    bookmarks = []
    for index in visibles:

        # get cell
        cell = cells[index]

        # check for display data
        graphic = False
        if any([entry['output_type'] == 'display_data' for entry in cell.setdefault('outputs', [])]):
            # switch graphic to true
            graphic = True

        # determine total lines of text in source
        source = cell['source']
        for line in source:
            # assume 100 characters per line
            lines += int(len(line) / 100)

        # compare to criterion
        if accumulation + lines > criterion or graphic:

            # add to scrolls and reset
            bookmarks.append(index)
            accumulation = lines
            lines = 0

            # for a graphic, make sure accumulation is already maxed
            if graphic:
                # add to accumulation
                accumulation = criterion + 1

        # otherwise
        else:

            # skip but add to total
            accumulation += lines

    return bookmarks


designate('describing cells')

# describe the cells
def describe(cells):
    """Describe the list of cells by printing cell summaries.

    Arguments:
        cells: list of dicts

    Returns:
        None
    """

    # get opens and scrolls
    opens = reveal(cells)
    scrolls = bookmark(cells, opens)

    # print cell metadata
    for index, cell in enumerate(cells):

        # construct stars
        stars = ''

        # check opens
        if index in opens:
            # add star
            stars += '*'

        # check scrolls
        if index in scrolls:
            # add star
            stars += '*'

        # print metadata
        print(' \n{} cell {}:'.format(stars, index))
        print(cell['source'][0][:100])
        print(cell['metadata'])
        print([key for key in cell.keys()])

        # print outputs
        if 'outputs' in cell.keys():

            # print outputs
            print('outputs:')
            for entry in cell['outputs']:
                # print keys
                print('\t {}, {}'.format(entry['output_type'], [key for key in entry.keys()]))

    return None


designate('capturing screenshots subroutine')


# capture a screenshot from each scroll position
def capture(scrolls):
    """Capture a screenshot from each scroll position

    Arguments:
        scrolls: list of ints

    Returns:
        list of images
    """

    # scroll to each cell and take screenshots
    screenshots = []
    for index in scrolls:
        # scroll to each
        command = 'IPython.notebook.scroll_to_cell(' + str(index) + ')'
        display(Javascript(command))

        # wait for 1 second to give it time
        sleep(1)

        # take screenshot
        screenshot = ImageGrab.grab()
        screenshots.append(screenshot)

    return screenshots


designate('detecting edges subroutine')


# detect where a row of pixels changes to a color
def detect(row, left=True, intensity=250, depth=0.4, tolerance=7):
    """Detect at which index a row of pixels changes to a color.

    Arguments:
        row: numpy array of pixels
        left=True: boolean, approach from left side?
        intensity=250: int, the intensity of the pixel
        depth=0.4: float, how deep into page to search
        tolerance=7: int, error range on color

    Returns:
        int, the index of the color change
    """

    # get row length
    length = len(row)

    # set values from left
    if left:

        # set values
        direction = 1
        first = 0
        last = int(length * depth)

    # or from right
    else:

        # set values
        direction = -1
        first = length - 1
        last = length - int(length * depth)

    # begin with a step size of 100, then 10, then 1
    for step in (100, 10, 1):

        # go through each pixel
        indices = [index for index in range(first, last + 1 * direction, step * direction)]
        for index in indices:

            # look for equality for each color
            pixel = row[index]
            if all([color - tolerance < intensity < color + tolerance for color in pixel[:3]]):
                # edge is found
                break

        # prepare next step
        last = index
        first = index - (step * direction)

    return index


designate('discovering corners subroutine')


# detect where a row of pixels changes to a color
def discover(image, left=True, top=True, intensity=250, depth=0.8, tolerance=7):
    """Discover at which index there is a corner.

    Arguments:
        image: numpy array of rows of pixels
        left=True: boolean, approach from left side?
        top=True: boolean, approach from top side?
        intensity=250: int, the intensity of the pixel
        depth=0.4: float, how deep into page to search
        tolerance=7: int, error range on color

    Returns:
        int, the index of the color change
    """

    # get image height
    height = len(image)

    # set values from top
    if top:

        # set values
        direction = 1
        first = 0
        last = int(height * depth)

    # or from bottom
    else:

        # set values
        direction = -1
        first = height - 1
        last = height - int(height * depth)

    # begin with a step size of 100, then 10, then 1
    border = None
    for step in (100, 10, 1):

        # go through each row
        edges = []
        indices = [index for index in range(first, last + 1 * direction, step * direction)]
        for index in indices:
            # calculate the edge and append
            row = image[index]
            edge = detect(row, left=left, intensity=intensity, tolerance=tolerance)
            edges.append((index, edge))

        # find border on first pass:
        if not border:
            # count all edges and set the border at the most common
            counter = Counter([edge[1] for edge in edges])
            counter = [item for item in counter.items()]
            counter.sort(key=lambda item: item[1], reverse=True)
            border = counter[0][0]

        # find first index at the border
        borders = [edge for edge in edges if edge[1] == border]
        borders.sort(key=lambda pair: pair[1], reverse=not top)
        index = borders[0][0]

        # reset range
        last = index
        first = index - (step * direction)

    return index, border


designate('trimming borders from images subroutine')


# trim borders from a screenshot
def trim(image):
    """Trim the borders from an image.

    Arguments:
        image: png image

    Returns:
        png image
    """

    # convert to pixels
    pixels = numpy.array(image)

    # determine the frame
    top, left = discover(pixels)
    bottom, right = discover(pixels, left=False, top=False)

    # trim the image
    trimmed = numpy.array([row[left:right] for row in pixels[top:bottom]])

    return trimmed


designate('pixel determining if rows are alike subroutine')


# determine if two rows of pixels are slike
def alike(pixels, pixelsii, resolution=5, tolerance=7):
    """Are two rows of pixels alike?

    Arguments:
        pixels: numpy array
        pixelsii: numpy array
        resolution=5: integer, lowest pixel distance to compare
        tolerance=7: integer, tolerance for intensity comparisons

    Returns:
        boolean
    """

    # check pixels until a mismatch is found or resolution is reached
    answer = True
    step = 100
    while answer and step > resolution:

        # check all pixels in subset
        for pixel, pixelii in zip(pixels[::step], pixelsii[::step]):

            # if any of the colors don't match
            if any([abs(int(color) - int(colorii)) > tolerance for color, colorii in zip(pixel, pixelii)]):
                # they are not equal
                answer = False
                break

        # decrease step
        step = step // 2

    return answer


designate('aligning up two images subroutine')


# find where two images line up
def align(image, imageii, position=0, start=0, down=True, same=True):
    """Align one image with another by finding the row on the first a row on the second matches.

    Arguments:
        image: numpy array
        imageii: numpy array
        position=0: int, the index of the row of imageii for matching
        start=0: int, index of row to begin at
        down=True: boolean, search downward?
        same=True: boolean, search for alike rows?

    Returns:
        int, the row
    """

    # get the pixels for comparison
    pixels = imageii[position]

    # get indices
    indices = [index for index, _ in enumerate(image)]

    # reverse
    if down:

        # only use starting indices
        indices = [index for index in indices if index >= start]

    # otherwise
    else:

        # reverse
        indices = [index for index in indices if index <= start]
        indices.reverse()

    # start from the top
    for index in indices:

        # check for equality to the condition
        rows = image[index: index + 5]
        rowsii = imageii[position: position + 5]
        if all([(alike(row, rowii) == same) for row, rowii in zip(rows, rowsii)]):
            # match is found
            break

    return index


designate('stitching together images subroutine')


# stitch together images
def stitch(images):
    """Stitch together multiple images by lining them up.

    Arguments:
        images: list of numpy arrays.

    Returns:
        numpy array
    """

    # set first image
    image = images[0]

    # make all white image
    white = numpy.array([[[250, 250, 250, 250]] * len(image[0])])

    # go through additional images
    for imageii in images[1:]:
        # find the highest row in second image
        highest = align(imageii, white, same=False)

        # find where this row aligns with original
        alignment = align(image, imageii, position=highest, start=len(image) - len(imageii))

        # stitch together
        image = numpy.concatenate((image[:alignment], imageii[highest:]))

    return image


designate('paginating image subroutine')


# paginate stitched image
def paginate(image):
    """Paginate an image by breaking up at white spaces.

    Arguments:
        image: numpy array

    Returns:
        list of numpy arrays
    """

    # set margin in pixels
    margin = 50

    # define ideal height, assuming 8 1/2 x 11
    width = len(image[0])
    height = int(11 * width / 8.5)

    # define white
    white = numpy.array([[[255, 255, 255, 255]] * width])

    # cut into pages
    pages = []
    while len(image) > height:
        # get bottom
        bottom = min([height, len(image) - 1])

        # find the closest white row
        closest = align(image, white, start=bottom, down=False)

        # cut the page
        page = numpy.concatenate((image[:closest], numpy.repeat(white, [height], axis=0)))
        page = page[:height]
        page = page.astype('uint8')
        pages.append(page)

        # reassemble image
        image = numpy.concatenate((numpy.repeat(white, [margin], axis=0), image[closest:]))

    # add last page
    page = numpy.concatenate((image, numpy.repeat(white, [height], axis=0)))
    page = page[:height]
    page = page.astype('uint8')
    pages.append(page)

    return pages


designate('assembling pdf pages subroutine')


# assemble pdf pages
def assemble():
    """Assemble the pdf pages from screenshots.

    Arguments:
        None

    Returns:
        images
    """

    # scan cells for metadata
    cells = scan()

    # discover open cells indices
    opens = reveal(cells)

    # bookmark cells to scroll to
    scrolls = bookmark(cells, opens)

    # get screenshots
    screenshots = capture(scrolls)

    # get images by trimming each screenshot
    images = [trim(screenshot) for screenshot in screenshots]

    # stitch together
    stitched = stitch(images)

    # paginate
    # pages = paginate(stitched)

    return images, stitched


designate('generating pdf')

# create push button linked to output
output = Output()
button = Button(description='Generate PDF')
display(button, output)


# function to export to csv
def generate(_):
    """Export the data into a csv file.

    Arguments:
        None

    Returns:
        None
    """

    # get images from acreenshots
    images = asseble()

    return None


# add button click command
button.on_click(generate)

